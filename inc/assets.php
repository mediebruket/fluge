<?php

add_action( 'wp_enqueue_scripts', 'fluge_enqueue_scripts' );
function fluge_enqueue_scripts() {

  // Stop execution if dist directory doesn't exist
  if ( ! is_dir(get_template_directory() . '/dist') ) {
    return;
  }

  $suffix = '.min';

  // Load unminified scripts when debug is on
  if ( WP_DEBUG === true ) {
    $suffix = '';
  }

  wp_register_style(
    'fluge-style',
    get_stylesheet_directory_uri() . '/dist/stylesheets/style' . $suffix . '.css',
    array(),
    filemtime( get_template_directory() . '/dist/stylesheets/style' . $suffix . '.css' )
  );
  wp_enqueue_style( 'fluge-style' );

  $script_name = 'scripts';
  wp_enqueue_script(
    'fluge-script',
    get_stylesheet_directory_uri() . '/dist/javascripts/' . $script_name . $suffix . '.js',
    array( 'jquery' ),
    filemtime( get_template_directory() . '/dist/javascripts/' . $script_name . $suffix . '.js' ),
    true
  );

}