<?php

function fluge_setup() {
   add_theme_support( 'title-tag' );
   add_theme_support( 'post-thumbnails' );
   add_theme_support( 'html5', array( 'search-form', 'gallery' ) );
}
add_action( 'after_setup_theme', 'fluge_setup' );
