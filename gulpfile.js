var gulp = require( 'gulp' ),
  autoprefixer = require( 'gulp-autoprefixer' ),
  browsersync = require( 'browser-sync' ),
  cache = require( 'gulp-cache' ),
  cleancss = require('gulp-clean-css');
  del = require('del');
  imagemin = require( 'gulp-imagemin' ),
  include = require( 'gulp-include' ),
  plumber = require( 'gulp-plumber' ),
  rename = require( 'gulp-rename' ),
  sass = require( 'gulp-sass' ),
  uglify = require( 'gulp-uglify' ),
  watch = require( 'gulp-watch' );

var onError = function( err ) {
  console.log( 'An error occurred:', err.message );
  this.emit( 'end' );
};

var src = 'src/';
var dist = 'dist/';

var config = {
  browsersync: {
    files: ['**/*.php', dist + '**', '!'+ dist + '**.map'],
    notify: true,
    open: true,
    port: 3000,
    proxy: 'sites/devsite', // Specify the URL to local development site
    server: false,
    watchOptions: {
      debounceDelay: 2000
    }
  },
  dist: {
    stylesheets: dist + 'stylesheets',
    javascripts: dist + 'javascripts',
    images: dist + 'images'
  },
  src: {
    stylesheets: src + 'stylesheets',
    javascripts: src + 'javascripts',
    images: src + 'images'
  }
};

gulp.task( 'scss', function() {
  return gulp.src( config.src.stylesheets + '/**/*.scss' )
    .pipe( plumber( { errorHandler: onError } ) )
    .pipe( sass() )
    .pipe( autoprefixer( 'last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4' ) )
    .pipe( gulp.dest( config.dist.stylesheets ) ) // Generate uniminified for debugging
    .pipe( cleancss() )
    .pipe( rename( { suffix: '.min' } ) )
    .pipe( gulp.dest( config.dist.stylesheets ) )
    ;
} );

gulp.task( 'javascripts', function() {
  return gulp.src( config.src.javascripts + '/**/*.js' )
    .pipe( plumber( { errorHandler: onError } ) )
    .pipe( include() )
    .pipe( rename( { basename: 'scripts' } ) )
    .pipe( gulp.dest( config.dist.javascripts ) )
    .pipe( uglify() )
    .pipe( rename( { suffix: '.min' } ) )
    .pipe( gulp.dest( config.dist.javascripts ) )
    ;
} );

gulp.task('images', function() {
  return gulp.src( config.src.images + '/**/*.{jpg,jpeg,png,gif,svg}' )
    .pipe( plumber( { errorHandler: onError } ) )
    .pipe( cache( imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }) ) )
    .pipe( imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }) )
    .pipe( gulp.dest( config.dist.images ) )
    ;
});

gulp.task('clean', function() {
  return del([config.dist.stylesheets, config.dist.javascripts, config.dist.images])
    ;
});

gulp.task( 'watch', ['scss', 'javascripts', 'images'], function() {
  watch( config.src.stylesheets + '/**/*.scss', function() {
    gulp.start('scss');
  });
  watch( config.src.javascripts + '/**/*.js', function() {
    gulp.start('javascripts');
  });
  watch( config.src.images + '/**/*.{jpg,jpeg,png,gif,svg}', function() {
    gulp.start('images');
  });
});

gulp.task('build', ['scss', 'images', 'javascripts']);

gulp.task('browsersync', ['watch'], function() {
  browsersync(config.browsersync);
});


gulp.task('default', ['watch']);
