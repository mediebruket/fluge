# Fluge #

Wordpress starter theme with Bower, Gulp and Npm for package management and asset compilation.

## Requirements ##

* node.js
* npm (included with node.js)

## Installation ##

* Install node.js. Download installer from https://nodejs.org/download/
* run `git clone git@bitbucket.org:mediebruket/fluge.git` inside wp-content/themes
* run `npm install` inside theme directory
* run `npm run bower install` inside theme directory
* Compile your assets and watch by running `npm run gulp watch`
* In order to use Browsersync, set the config.browsersync.proxy variable to the url of the development site

## Todo ##

* ~~Autoprefixer~~
* ~~Browsersync~~
* ~~Remove gulp-clean dependency, use node.js `del` module instead https://github.com/gulpjs/gulp/blob/master/docs/recipes/delete-files-folder.md~~
* Gulp workflow for working with SVG. Alternative to Grunticon.

## Credits ##

* https://codeable.io/community/speed-up-your-theme-development-with-gulp/
* https://github.com/synapticism/wordpress-gulp-bower-sass/
* http://markgoodyear.com/2014/01/getting-started-with-gulp/
